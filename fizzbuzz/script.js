'use strict';

var paragrafo = document.querySelector('#resultado');
var campo = document.querySelector('input');

campo.addEventListener('input', function(){
  var numeros = [];
  for (var i = 1; i <= this.value; i++) {
    if ((i % 15) == 0) {
      numeros.push('fizzbuzz');
    } else if ((i % 3) == 0) {
      numeros.push('fizz');
    } else if ((i % 5) == 0) {
      numeros.push('buzz');
    } else {
      numeros.push(i);
    }
  }

  paragrafo.innerHTML = numeros.join('<br>');
});
