# Próxima atividade #

### Fatores primos ###

* O usuário somente pode digitar valores inteiro entre 2 e 1000.
* Deve exibir os fatores primos desse número, por exemplo:

## O usuário digita 56:
* 56 - 2
* 28 - 2
* 14 - 2
* 7  - 7
* 1

* Deve ser exibido: 2, 2, 2, 7

## O usuário digita 732:
* 732 - 2
* 366 - 2
* 183 - 3
* 61  - 61
* 1

* Deve ser exibido: 2, 2, 3, 61

## O usuário digita 2:
* 2 - 2

* Deve ser exibido: 2